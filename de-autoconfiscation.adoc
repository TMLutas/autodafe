= De-Autocoonfiscation HOWTO
:description: How to free your project from the clammy grip of autotools
:keywords: autotools
by Eric S. Raymond

This document describes how to sever a project using autotools
from the autotools machinery, leaving equivalent bare Makefiles
in place.

== Motivation

In early April of 2024, an extremely serious compromise of ssh(1)
using Trojan code inserted by a trusted committer into the LZMA
library was spotted only weeks before it might have entered general
distribution. It is not easy to hide a back door in publicly
auditable open-source code, but the attacker exploited the fact
that nobody ever wants to look at or touch autotools recipes
and their generated scripts if they can possibly avoid it.

This brought into sharp focus problems with autotools that have been
accumulating for decades. Autotools recipes are notoriously difficult
to read and modify. The two-phase build (generation of a Makefile
which is then executed to build the project) makes debugging arduous.
Programmers inexperienced with autotools can't audit a recipe at all,
and those with enough experience tend to have developed a powerful
flinch reflex that keeps them from actually doing it.

This makes the jungle of recipe and intermediate-product files in an
autotools recipe ideal places to hide exploit components. Attempting
to add anti-Trojaning checks would only add more complications, so the
path of sanity is simply to scrap autotools for a simpler build
flow.

The challenge then, is how best to burn away the jungle foliage
without breaking project builds.

== Background

An autotools run has two products: a config.h file and a Makefile.
There are a great many complications around the specifications used to
generate these products that need not be described here.

The config.h file is a list of preprocessor #defines meant to be used
in the guards of preocessor #ifdef/#endif and #ifndef/#endif sections.
These are typically used for select inclusion of header files and code
that support platform-specific portability shims and sometimes
optional features.

The Makefile is the final form of the build recipe - what you actually
run make(1) against to generate your binaries and other artifacts such
as rendered forms of documentation. The most important platform
dependency in the Makefile is usually the details of how to invoke
your compiler and linker. The recipe may also test for optional or
variant libraries to be linked into your binaries.

At generation time, autotools scripts perform an elaborate sequence
of checks to discover the capabilities of the host system.  When
autotools was first fielded it was not uncommon for the config.h
files of large projects to set dozens of guard #defines.

The important thing to know is that config.h/Makefile generation is
complex mainly because of API portability issues that have been mostly
obsolesced by increasing standardization around C99 and SuSv2 (the
Single Unix Standard, version 2, descended from POSIX).
Standardization on GNU Make has also addressed some (though not all)
of the portability issues in Makefiles.

== Goals

In this HOWTO, we will describe ways to abolish config.h entirely and
transform each of your autotools-generated product Makefile into an
equivalent bare Makefile that supports the same build actions and is
more amenable to being read and modified.  Once this process is
complete, all of the specifications and scripts previously used to
generate config.h and the Makefile can be discarded.

For simple projects this transformation is nearly trivial. If your
project is not simple, jumping this gap may require significant
skill and judgment. The conversion procedure is structured in such a
way that you don't have to commit to the entire process in one go,
but can do it in decomposable steps.

== Commitment

The commitment you have to make to start this process is this: _you
are going to throw away the 20th century._ All ancient portability
shims will be discarded. From now on, you will assume that C has
bools, that memcpy(3) exists, that terminals speak termios(3), that
you have stdarg(3) support, and so forth.  This is what you need to
do to banish autotools.

Unix programmers have very conservative instincts.  We tend to cling
to ancient porting kludges as though we might at any point suddenly
need to build our code on a MicroVAX from 1989. In reality, the
standardization people won their war in the mid-2000s, and the xz
Trojan has revealed that carrying around all that historical baggage
is an actual liability that we need to get shut of.

It is not insignificant that tossing out this baggage lightens your
project's maintainence load. Bugs accumulate around these kludges.

== Tradeoffs

There are some drawbacks to this procedre.

* You sacrifice portability to ancient big-iron Unixes.

* De-obfuscation does not make the converted bare makefile as
  readable as one might ideally wish; makemake prioritizes provable
  correctness of the transformation over a more aggressive approach
  that might improve this.

* You lose the ab8lity to set system directories for installatiom
  to non-default versions using optionms of configure; instead,
  you have to tweak the variable definitions in your nakefiles
  directly.

== Steps

Nake a branch in your repository so you can experiment safely.

=== Deconditionalize C99/POSIX features

Your first step is to apply ifdex to get a report about the #defines
your code uses for guarded sections.  Usually you'll just do this
by running "ifdex ." in your top-level directory.

Your next step (which doesn't yet require changing your build recipe
at all) is to eliminate as many of these as you can.  Here is a
representative ifdex report on guards you can simply delete
(retaining the code they are wrapped around) because every C99/POSIX
system supports the entry points they are guarding:

----
HAVE_FCNTL_H
HAVE_MEMORY_H
HAVE_STDARG_H
HAVE_STDBOOL_H
HAVE_STDINT_H
HAVE_STRDUP
HAVE_STRING_H
HAVE_UNISTD_H
HAVE_VPRINTF
----

In very old code you might see "SYSV".  Usually you can simply delete
this guard (retaining the code) because the entry points it uses
are in POSIX.

Another class of guards you can get rid of easily is for
architecture-specific code.  Autotools inspects your system
and sets up its own #defines, but it's better to toss
those out and use the ones gcc(1) and clang(1) provide.

The correctness of these changes is easy to verify. Since the guarded
code is probably your normal compilation path on any
C99/SuSv2-complioant Unix system, if it still compiles after guard
removal you can be confident of not having introduced bugs.

=== Throw away obsolete code sections

Here's are example of #defines guarding non-C99 porting shims that you
should discard:

----
HAVE_VARARGS_H
HAVE_STRINGS_H
----

The varargs(3) interface was a competitor to stdarg(3) that is now
long obsolete. Your codebase now assumes stdarg(3), so it's unneeded.
Similarly, HAVE_STRINGS_H guards code using the obsolete BSD strings
library

=== Start removing <config.h> inclusions

Once you've deconditionalized or removed all code guarded by HAVE_
definitions, start removing <config.h> inclusions and test-compiling
to see if anything breaks. Every one of these you can remove is a
victory.  When you can remove them all, you have accomplished the
trickiest part of your conversion.

What to do when removing inclusion of <config.h> breaks compilation of
a module is difficult to specify in all possible odd cases. Some skill
may be required.

However, we can describe a pattern for handling simple cases.  One
common sticking point that we'll use as an example is VERSION. What
you need to do is add something like this early in your Makefile.am:

----
AM_CFLAGS += -DVERSION=$(VERSION)
-----

What you're doing here is exploiting the fact that autotools also
sets VERSION as a variable in your generated Makefile.  This
definition picks up that value and adds it to CFLAGS, passing
it in by a route other than <config.h> inclusion.

Walk through all cases like this until you can remove the last
<config.h> inclusion.

=== De-obfuscate your generated Makefiles

To de-obfuscate your Makefiles, run this command in your top-level
directory:

----
./makemake .
----

This will walk through your code tree looking for Makefiles, moving
them so they are named Makefile.bak, and then filtering them and
sending the output to Makefile.

The de-obfuscation transform is not very complex.  It consists of (1)
selectively expending Make variables in the same way Make does, and
(2) deleting things (variable definitions, and productions to make
autotools intermediate products) that are mo longer useful.

De-obfuscation does mean that some configuration values that used
to be discovered by autotools are now static settings in the
bare Makefile. Among these are build_cpu and host_os.  If you need
these, you will have to set them by hand for each build.

You can use the -x optopn to declare a comma-separated list of
productions to discard. The advabtage over simply removing them
after de-obfuscation is that if any variables become unreferenced
due to removal, those setting will be removed as well.

=== Test your build

After the preceding step, you have in theory severed your dependency on the
autotools machinery.  To know whether you have done so in practice,
make clean followed by make; watch for compilation errors and run your
normal test suite.

Any failure at this point probably means makemake has a bug and you
should file an issue with the maintainers.

This would be a good time to check in your changes.

=== Clean up files

To finally jettison autotools, run this command in your top-level
directory.

----
./makemake -c .
----

This will remove all autotools scripts and intermediate-product files
except your Makefiles, which you modified in the previous step. It
will remove Makefile.bak files.

It'as a good idea to test your build after this step.  You're watching
for hidden dependencies on missing intermediate projects.

=== Clean up your Makefiles

After the previous step, you have ensured that no relative of the xz
crack is lurging in the undegrowth of your bnuild recipe. It now
consists of Makefiles that are relatively easy to inspect.

But "relatively" is not "absolutely". The productions that autotools
generates for utility targets such as "clean" and "dist" can be pretty
grotty; you may want to replace them with more customized and readable
equivalents.

A possible source of problems is some variable values having been generated
as absolute pathnames. You will probably want to fix these sio they are
relative to $(PWD).

== Frequently Asked Questions

=== Why bare makefiles and not My Favorite Heavyweight Build System?

I have been asked this question on X about cmake, mkmk, meson, scons,
bazel, and doubtless others I have forgotten.  The answers for all of
them are the same.

1. Make is everywhere, and anybody using an autotools build already
has it installed.

2. It's much easier to transform a generated Makefile into another
Makefile in a way that is provably correct than it would be to try to
translate that generated Makefile into a different
build-specification language. Attempting the latter would be just
begging for trouble.

3. Even if translation to a different build engine were technically
feasible, gluing it into makemake would be the wrong thing to do. Better
to perform an information-preserving transformation to bare makefiles
(doing one thing well) that can be fed to a separate translation
utility (doing another one thing well).

4. Aside from all those objections, I have a lot of experience with
heavyweight buiold systems and it has soured me on them. They tend to
be badly designed, overengineered, poorly documented, and attractors
of complexity and maintenance hassles. Bare makefiles have their
flaws, but as of 2024 all the alternatives are all still worse.

Anyone who is moved to respond to the previous complaint with "Try my
favorite build system you haven't mentioned! It is really super-duper
keen and I am sure you will love it and bow before its awesomeness!"
is advised against saying this to me face-to-face when I am within
reach of an object I could club you to death with.

== Revision history

0.1::
    Initial version

